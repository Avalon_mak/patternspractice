package com.mak;

import com.mak.codec.Codec;
import com.mak.model.Entity;

import java.io.ByteArrayInputStream;
import java.util.List;

public class App {
    public static void main(String[] args) {
        EntityCodecProducer entityProducer = new EntityCodecProducer();
        FileTypeCodecProducer fileTypeProducer = entityProducer.getFileTypeProducer("abonent");
        Codec codec = fileTypeProducer.getCodec("xml");
        List<? extends Entity> result = codec.decode(new ByteArrayInputStream(new byte[0]));
        for (Entity entity : result) {
            System.out.println(entity.getUUID());
        }
    }
}

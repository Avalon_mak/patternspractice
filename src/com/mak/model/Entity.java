package com.mak.model;

public interface Entity {
    String getUUID();
    void setUUID(String UUID);
}

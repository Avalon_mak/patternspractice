package com.mak.model;

public class Abonent implements Entity {
    private String entityName;
    private Integer number;

    @Override
    public void setUUID(String UUID) {
        this.entityName = UUID;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Override
    public String getUUID() {
        return entityName;
    }
}

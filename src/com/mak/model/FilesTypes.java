package com.mak.model;

public enum FilesTypes {
    XML,
    CSV,
    JSON
}

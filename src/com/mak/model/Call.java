package com.mak.model;

public class Call implements Entity {

    private Integer callDurability;
    private String entityName;

    public Integer getCallDurability() {
        return callDurability;
    }

    public void setCallDurability(Integer callDurability) {
        this.callDurability = callDurability;
    }

    @Override
    public void setUUID(String UUID) {
        this.entityName = UUID;
    }

    @Override
    public String getUUID() {
        return entityName;
    }
}

package com.mak.model;

public class SMS implements Entity {

    private String entityName;

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    private String messageText;

    @Override
    public void setUUID(String UUID) {
        this.entityName = UUID;
    }

    @Override
    public String getUUID() {
        return entityName;
    }
}

package com.mak;

import com.mak.codec.CodecFactory;


public class EntityCodecProducer {

    private final CodecFactory codecFactory = CodecFactory.getInstance();

    public FileTypeCodecProducer getFileTypeProducer(String entityType) {
        FileTypeCodecProducer fileTypeProducer = new FileTypeCodecProducer(codecFactory.getEntityCodecs(entityType));
        return fileTypeProducer;
    }
}

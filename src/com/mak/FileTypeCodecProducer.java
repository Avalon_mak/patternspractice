package com.mak;

import com.mak.codec.Codec;

import java.util.HashMap;
import java.util.Map;

public class FileTypeCodecProducer {

    private Map<String, Class<? extends Codec>> codecs = new HashMap<>();


    public FileTypeCodecProducer(Map<String, Class<? extends Codec>> codecs) {
        this.codecs = codecs;
    }

    public Codec getCodec(String fileType) {
        Codec result = null;
        try {
            result = codecs.get(fileType.toUpperCase()).newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }
}

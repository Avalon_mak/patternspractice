package com.mak.codec.abonent;

import com.mak.annotations.EntityType;
import com.mak.annotations.FileType;
import com.mak.codec.Codec;
import com.mak.model.Abonent;
import com.mak.model.Entity;
import com.mak.service.Writer;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


@FileType(fileType = "CSV")
@EntityType(entityName = "ABONENT")
public class AbonentCsvCodec implements Codec {

    @Override
    public List<? extends Entity> decode(InputStream binaryData) {
        Writer writer = new Writer();
        Abonent abonent = new Abonent();
        abonent.setNumber(1);
        abonent.setUUID("ABONENT_CSV");
        List<Abonent> result = new ArrayList<>();
        result.add(abonent);
        for (Entity e: result) {
            writer.updateAbonentRecord(e);
        }
        return result;
    }
}

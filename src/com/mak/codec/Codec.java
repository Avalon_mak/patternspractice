package com.mak.codec;

import com.mak.model.Entity;

import java.io.InputStream;
import java.util.List;

public interface Codec {

    List<? extends Entity> decode(InputStream binaryData);

}

package com.mak.codec.call;

import com.mak.annotations.EntityType;
import com.mak.annotations.FileType;
import com.mak.codec.Codec;
import com.mak.model.Call;
import com.mak.model.Entity;
import com.mak.service.Writer;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@FileType(fileType = "XML")
@EntityType(entityName = "CALL")
public class CallXmlCodec implements Codec {

    @Override
    public List<? extends Entity> decode(InputStream binaryData) {
        Writer writer = new Writer();
        Call call = new Call();
        call.setCallDurability(13);
        call.setUUID("CALL_XML");
        List<Call> result = new ArrayList<>();
        result.add(call);
        for (Entity e: result) {
            writer.updateCallRecord(e);
        }
        return result;
    }
}

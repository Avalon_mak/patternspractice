package com.mak.codec.sms;

import com.mak.annotations.EntityType;
import com.mak.annotations.FileType;
import com.mak.codec.Codec;
import com.mak.model.SMS;
import com.mak.model.Entity;
import com.mak.service.Writer;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@FileType(fileType = "CSV")
@EntityType(entityName = "SMS")
public class SmsCsvCodec implements Codec {

    @Override
    public List<? extends Entity> decode(InputStream binaryData) {
        Writer writer = new Writer();
        SMS sms = new SMS();
        sms.setMessageText("SMS_CSV");
        sms.setUUID("SMS_CSV");
        List<SMS> result = new ArrayList<>();
        result.add(sms);
        for (Entity e: result) {
            writer.updateSMSRecord(e);
        }
        return result;
    }
}

package com.mak.codec.sms;

import com.mak.annotations.EntityType;
import com.mak.annotations.FileType;
import com.mak.codec.Codec;
import com.mak.model.Entity;
import com.mak.model.SMS;
import com.mak.service.Writer;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@FileType(fileType = "JSON")
@EntityType(entityName = "SMS")
public class SmsJsonCodec implements Codec {

    @Override
    public List<? extends Entity> decode(InputStream binaryData) {
        Writer writer = new Writer();
        SMS sms = new SMS();
        sms.setMessageText("SMS_JSON");
        sms.setUUID("SMS_JSON");
        List<SMS> result = new ArrayList<>();
        result.add(sms);
        for (Entity e: result) {
            writer.updateSMSRecord(e);
        }
        return result;
    }
}

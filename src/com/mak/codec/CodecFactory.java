package com.mak.codec;

import com.mak.annotations.EntityType;
import com.mak.annotations.FileType;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CodecFactory {

    private final String SCAN_PACKAGE = "com.mak";

    private static final CodecFactory INSTANCE = new CodecFactory();

    private Map<String, Map<String, Class<? extends Codec>>> codecs = new HashMap<>();

    private CodecFactory() {
        codecs = this.collectCodecs();
    }

    public static CodecFactory getInstance() {
        return INSTANCE;
    }

    private Map<String, Map<String, Class<? extends Codec>>> collectCodecs() {

        List<Class<? extends Codec>> implementations = new ArrayList<>();
        new FastClasspathScanner(SCAN_PACKAGE)
                .matchClassesImplementing(Codec.class,
                        c -> implementations.add(c))
                .scan();

        for (Class<? extends Codec> i : implementations) {
            EntityType et = i.getAnnotation(EntityType.class);
            Map<String, Class<? extends Codec>> sortedImplementations = new HashMap<>();
            String entityName = et.entityName();
            for (Class<? extends Codec> codec : implementations) {
                if (codec.getAnnotation(EntityType.class).entityName().equals(entityName)) {
                    FileType ft = codec.getAnnotation(FileType.class);
                    sortedImplementations.put(ft.fileType(), codec);
                }
            }
            this.codecs.put(entityName, sortedImplementations);
        }
        return this.codecs;
    }

    public Map<String, Class<? extends Codec>> getEntityCodecs(String entityType) {
        return this.codecs.get(entityType.toUpperCase());
    }
}
